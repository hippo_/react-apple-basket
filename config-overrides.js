const {
  override,
  removeModuleScopePlugin,
  addLessLoader,
  addDecoratorsLegacy
} = require("customize-cra");

module.exports = {
  webpack: override(
    removeModuleScopePlugin(),
    addDecoratorsLegacy(),
    addLessLoader({
      lessOptions: {
        javascriptEnabled: true
      }
    })
  )
};
