import { Icon, MoonIcon, SunIcon } from "@chakra-ui/icons";
import {
  Box,
  Button,
  Center,
  Container,
  useColorMode,
  useColorModeValue,
  useTheme,
  Text,
  Flex,
  Link
} from "@chakra-ui/react";
import AppleItem from "AppleItem";
import { inject, observer } from "mobx-react";
import React, { FC } from "react";

interface TState {
  appleStore?: IAppleStore;
}
const App: FC<TState> = ({ appleStore }) => {
  const theme = useTheme();
  const { colorMode, setColorMode, toggleColorMode } = useColorMode();
  return (
    <Container
      marginTop={20}
      border="1px"
      borderStyle="solid"
      borderColor={useColorModeValue("facebook.400", "telegram.400")}
      padding="0"
      borderRadius={4}
      display="flex"
      flexDirection="column"
      w="30rem"
    >
      <Center
        height="40px"
        borderBottom="1px"
        borderBottomStyle="dashed"
        borderColor={useColorModeValue("facebook.400", "telegram.400")}
        position="relative"
      >
        <Text
          fontSize="1xl"
          color={useColorModeValue("facebook.400", "telegram.400")}
        >
          苹果篮子
        </Text>
        <Box
          padding="-0.5"
          position="absolute"
          right="10"
          top="50%"
          transform="translateY(-50%)"
          cursor="pointer"
        >
          {colorMode === "light" && (
            <MoonIcon color="facebook.400" onClick={toggleColorMode} />
          )}
          {colorMode === "dark" && (
            <SunIcon color="telegram.200" onClick={toggleColorMode} />
          )}
          <Link
            href="https://github.com/HippoIvan/react-apple-basket"
            marginLeft="4"
          >
            <Icon
              viewBox="0 0 20 20"
              color={useColorModeValue("facebook.200", "telegram.200")}
            >
              <path
                fill="currentColor"
                d="M10 0a10 10 0 0 0-3.16 19.49c.5.1.68-.22.68-.48l-.01-1.7c-2.78.6-3.37-1.34-3.37-1.34-.46-1.16-1.11-1.47-1.11-1.47-.9-.62.07-.6.07-.6 1 .07 1.53 1.03 1.53 1.03.9 1.52 2.34 1.08 2.91.83.1-.65.35-1.09.63-1.34-2.22-.25-4.55-1.11-4.55-4.94 0-1.1.39-1.99 1.03-2.69a3.6 3.6 0 0 1 .1-2.64s.84-.27 2.75 1.02a9.58 9.58 0 0 1 5 0c1.91-1.3 2.75-1.02 2.75-1.02.55 1.37.2 2.4.1 2.64.64.7 1.03 1.6 1.03 2.69 0 3.84-2.34 4.68-4.57 4.93.36.31.68.92.68 1.85l-.01 2.75c0 .26.18.58.69.48A10 10 0 0 0 10 0"
              ></path>
            </Icon>
          </Link>
        </Box>
      </Center>
      <Flex
        height="80px"
        borderBottom="1px"
        borderBottomStyle="dashed"
        borderColor={useColorModeValue("facebook.400", "telegram.400")}
        flexDirection="row"
        color={useColorModeValue("facebook.400", "telegram.400")}
      >
        <Flex
          flex="1"
          padding={15}
          // paddingLeft={100}
          flexDirection="column"
          justifyContent="space-between"
          alignItems="center"
        >
          <Text>当前</Text>
          <Text color={useColorModeValue("orange.400", "purple.400")}>
            {appleStore?.unCompute.remain.num}个苹果，
            {appleStore?.unCompute.remain.weight}克
          </Text>
        </Flex>
        <Flex
          flex="1"
          padding={15}
          flexDirection="column"
          justifyContent="space-between"
          alignItems="center"
        >
          <Text>已吃掉</Text>
          <Text color={useColorModeValue("orange.400", "purple.400")}>
            {appleStore?.unCompute.eat.num}个苹果，
            {appleStore?.unCompute.eat.weight}克
          </Text>
        </Flex>
      </Flex>
      <Container
        flex="1"
        paddingInline="20"
        paddingBlock="4"
        display="flex"
        justifyContent="center"
        flexDirection="column"
      >
        {appleStore?.unRemain?.map((p) => (
          <AppleItem key={p.id} basket={p} onEat={appleStore?.onEat} />
        ))}
        <Button isLoading={appleStore?.loading} onClick={appleStore?.addApple}>
          摘苹果
        </Button>
      </Container>
    </Container>
  );
};
App.displayName = "APP";
export default inject("appleStore")(observer(App));
