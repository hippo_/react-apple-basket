import {
  Box,
  useColorModeValue,
  Image,
  Flex,
  Text,
  Button
} from "@chakra-ui/react";
import React, { memo } from "react";
import AppleIcon from "assets/images/apple.png";
type TAppleItem = { basket: IBasketState; onEat?: (e: number) => void };
const AppleItem = memo<TAppleItem>(({ basket: { id, weight }, onEat }) => {
  return (
    <Flex
      borderWidth={1}
      borderStyle="solid"
      borderColor={useColorModeValue("facebook.400", "telegram.400")}
      height="80px"
      flex="1"
      marginBottom={4}
      alignItems="center"
      borderRadius="4px"
      paddingInline="4"
      paddingBlock="2"
      justifyContent="space-between"
    >
      <Image src={AppleIcon} />
      <Box>
        <Text color={useColorModeValue("facebook.400", "telegram.400")}>
          红苹果 - {id}号
        </Text>
        <Text color={useColorModeValue("orange.400", "purple.400")}>
          {weight}克
        </Text>
      </Box>
      <Button colorScheme="blue" onClick={onEat?.bind(null, id)}>
        吃掉
      </Button>
    </Flex>
  );
});

AppleItem.displayName = "AppleItem";
export default AppleItem;
