interface IAppleStore {
  basketState: IBasketState[];
  unRemain: IBasketState[];
  loading: boolean;
  addApple: () => void;
  unCompute: {
    eat: { num: number; weight: number };
    remain: { num: number; weight: number };
  };
  onEat: (e: number) => void;
}
interface IBasketState {
  id: number;
  weight: number;
  isEat: boolean;
}
