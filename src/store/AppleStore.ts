import { makeAutoObservable, runInAction } from "mobx";

class AppleStore implements IAppleStore {
  public basketState: IBasketState[] = [];
  public loading = false;
  constructor() {
    makeAutoObservable(this, {}, { autoBind: true });
  }
  get unCompute() {
    const eat = { num: 0, weight: 0 };
    const remain = { num: 0, weight: 0 };
    this.basketState.forEach((p) => {
      if (p.isEat) {
        eat.num += 1;
        eat.weight += p.weight;
      } else {
        remain.num += 1;
        remain.weight += p.weight;
      }
    });
    return { eat, remain };
  }
  get unRemain() {
    return this.basketState.filter((p) => !p.isEat);
  }
  async addApple() {
    runInAction(() => {
      this.loading = true;
    });
    await new Promise((resolve) => {
      setTimeout(() => {
        resolve(true);
      }, Math.round(1000 * Math.random()));
    });
    runInAction(() => {
      this.loading = false;
      this.basketState.push({
        id: (this.basketState[this.basketState.length - 1]?.id ?? 0) + 1,
        weight: Math.round(180 * Math.random()),
        isEat: false
      });
    });
  }

  onEat(id: number) {
    for (const i in this.basketState) {
      if (this.basketState[i].id === id) {
        this.basketState[i].isEat = true;
        break;
      }
    }
  }
}

export default AppleStore;
