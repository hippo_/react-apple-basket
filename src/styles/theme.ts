import { extendTheme } from "@chakra-ui/react";
import theme from "@chakra-ui/theme";

theme.config.initialColorMode = "dark";
theme.config.useSystemColorMode = false;

const colors = {};
export default extendTheme({ colors });
